# cargo-sonar

`cargo-sonar` help you to use the tools of the Rust community and report the information to Sonarcloud (or Sonarqube).
You can even set it up in a Continuous Integration so this report is automatically forwarded to Sonar.

# Table of contents
- [Installation](#installation)
  - [From source](#from-source)
  - [From crates.io](#from-cratesio)
  - [From Docker/Podman](#from-dockerpodman)
- [Use](#use)
- [Supported tools](#supported-tools)
  - [`cargo-clippy`](#cargo-clippy)
  - [`cargo-audit`](#cargo-audit)
  - [`cargo-deny`](#cargo-deny)
  - [`cargo-outdated`](#cargo-outdated)
  - [`cargo-udeps`](#cargo-udeps)
- [Examples](#examples)
- [Release](#release)
  - [Tagging](#tagging)
  - [Package on crates.io](#package-on-cratesio)
  - [Docker image](#docker-image)
- [Todo list](#todo-list)

# Installation
## From source

```
git clone https://gitlab.com/woshilapin/cargo-sonar
cd cargo-sonar
cargo install --path .
cargo sonar --help
```

## From crates.io

```
cargo install cargo-sonar
cargo sonar --help
```

## From Docker/Podman

```
export CONTAINER_ENGINE=docker # or CONTAINER_ENGINE=podman
${CONTAINER_ENGINE} pull docker.io/woshilapin/cargo-sonar
${CONTAINER_ENGINE} run docker.io/woshilapin/cargo-sonar --help
```

# Use

`cargo-sonar` is only a tool to convert reports from other tools into Sonar compatible report (see [Supported tools](#supported-tools)).
Once the Sonar report is generated, it can be sent to [sonarcloud.io](https://sonarcloud.io) or any [SonarQube](https://www.sonarqube.org/) instance with [`sonar-scanner`](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/).

First generate a report from any [supported tool](#supported-tools), for example `clippy`.

```
cargo clippy --message-format > my-clippy-report.json
```

Then convert this report.

```
cargo sonar --features clippy --clippy-path my-clippy-report.json
```

This creates a file `sonar.json`.
You can now configure `sonar-scanner` with `sonar.externalIssuesReportPaths=sonar.json` in your `sonar-project.properties` file.

# Supported tools
## `cargo-clippy`

```
cargo clippy --message-format=json -- --warn clippy::pedantic > clippy.json
```

## `cargo-audit`

```
cargo audit --json > audit.json
```

## `cargo-deny`

```
cargo deny --format json check 2> deny.json
```

Note that only `advisories` and `licenses` are supported at the moment.

## `cargo-outdated`

```
cargo outdated --depth 1 --format json > outdated.json
```

`--depth 1` is useful here since the conversion will not work on any dependency
of greater depth.

## `cargo-udeps`

```
cargo +nightly udeps --quiet --workspace --all-features --all-targets --output json > udeps.json
```

# Examples

The best example out there at the moment is the project `cargo-sonar` itself.
In the CI, you can see the generation of the [`clippy` report](https://gitlab.com/woshilapin/cargo-sonar/-/blob/main/.gitlab/clippy.yml) and the [`audit` report](https://gitlab.com/woshilapin/cargo-sonar/-/blob/main/.gitlab/audit.yml).
Then, `cargo sonar` is [executed](https://gitlab.com/woshilapin/cargo-sonar/-/blob/main/.gitlab/sonar.yml) followed by [`sonar-scanner`](https://gitlab.com/woshilapin/cargo-sonar/-/blob/main/.gitlab/sonar.yml) with the [`sonar-project.properties` configuration file](https://gitlab.com/woshilapin/cargo-sonar/-/blob/main/sonar-project.properties).
The final result can be seen on [sonarcloud.io](https://sonarcloud.io/project/overview?id=woshilapin_cargo-sonar).

# Release

All the release process is automated: each time you push a commit on `main` branch,
the next version is automatically deduce from the [conventional commit standard](https://www.conventionalcommits.org/en/v1.0.0/) since last tag.

Sometimes, the CI might get into a problem. If you need to switch to manual release, here are the steps.
Below, `1.2.3` is used as an example, please replace with the correct version.

## Tagging

```
cog bump --auto
```

## Package on crates.io

```
git checkout 1.2.3
cargo publish
```

## Docker image

```
git checkout 1.2.3
buildah bud --layers --tag woshilapin/cargo-sonar:1.2.3
buildah push woshilapin/cargo-sonar:1.2.3
```

# Todo list

- [ ] add `cargo-geiger` parsing
- [ ] create a Github Action (see [Docker Github Action](https://docs.github.com/en/actions/creating-actions/creating-a-docker-container-action))
