use serde::Serialize;

#[derive(Debug, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
#[allow(dead_code)]
pub enum Severity {
    Blocker,
    Critical,
    Major,
    Minor,
    Info,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
// NOTE: `Bug` is not yet used but is a valid value for the Sonar generic issue format
#[allow(dead_code)]
pub enum IssueType {
    Bug,
    Vulnerability,
    CodeSmell,
}
#[derive(Debug, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct TextRange {
    pub start_line: usize,
    pub end_line: usize,
    pub start_column: usize,
    pub end_column: usize,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Location {
    pub message: String,
    pub file_path: String,
    pub text_range: TextRange,
}
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Issue {
    pub engine_id: String,
    pub rule_id: String,
    pub severity: Severity,
    pub r#type: IssueType,
    pub primary_location: Location,
    pub secondary_locations: Vec<Location>,
}

#[derive(Debug, Default, Serialize)]
pub struct Report {
    issues: Vec<Issue>,
}

impl Report {
    pub fn len(&self) -> usize {
        self.issues.len()
    }
}

impl FromIterator<Issue> for Report {
    fn from_iter<T: IntoIterator<Item = Issue>>(iter: T) -> Self {
        Self {
            issues: iter.into_iter().collect(),
        }
    }
}

impl IntoIterator for Report {
    type Item = Issue;

    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.issues.into_iter()
    }
}

impl Extend<Issue> for Report {
    fn extend<T: IntoIterator<Item = Issue>>(&mut self, iter: T) {
        self.issues.extend(iter);
    }
}
