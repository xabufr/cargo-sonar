#![doc = include_str!("../README.md")]

#[cfg(feature = "audit")]
mod audit;
#[cfg(any(
    feature = "audit",
    feature = "deny",
    feature = "outdated",
    feature = "udeps"
))]
mod cargo;
mod cli;
#[cfg(feature = "clippy")]
mod clippy;
#[cfg(feature = "deny")]
mod deny;
#[cfg(feature = "outdated")]
mod outdated;
mod sonar;
#[cfg(feature = "udeps")]
mod udeps;

use std::fs::File;

use clap::Parser;
use eyre::{Context as _, Result};
use tracing::{info, Level};
use tracing_subscriber::{prelude::*, EnvFilter};

fn init_tracer() {
    let default_level = Level::INFO;
    let rust_log =
        std::env::var(EnvFilter::DEFAULT_ENV).unwrap_or_else(|_| default_level.to_string());
    let env_filter_subscriber = EnvFilter::try_new(rust_log).unwrap_or_else(|e| {
        eprintln!(
            "invalid {}, falling back to level '{}' - {}",
            EnvFilter::DEFAULT_ENV,
            default_level,
            e,
        );
        EnvFilter::new(default_level.to_string())
    });
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(env_filter_subscriber)
        .init();
}

fn main() -> Result<()> {
    init_tracer();
    color_eyre::install()?;
    let options = cli::Command::parse();

    #[cfg(any(
        feature = "audit",
        feature = "deny",
        feature = "outdated",
        feature = "udeps"
    ))]
    let lockfile = {
        use std::env::current_dir;
        let lockfile_path = current_dir()?.join("Cargo.lock");
        cargo::Lockfile::try_from(lockfile_path.as_path())?
    };
    let mut sonar_report = sonar::Report::default();
    for feature in &options.features {
        match feature {
            cli::Feature::Audit => {
                use crate::audit::Audit;
                let audit_report: sonar::Report =
                    Audit::new(&options.audit_path, &lockfile).try_into()?;
                sonar_report.extend(audit_report);
            }
            cli::Feature::Clippy => {
                use crate::clippy::Clippy;
                let clippy_report: sonar::Report = Clippy::new(&options.clippy_path).try_into()?;
                sonar_report.extend(clippy_report);
            }
            cli::Feature::Deny => {
                use crate::deny::Deny;
                let deny_report: sonar::Report =
                    Deny::new(&options.deny_path, &lockfile).try_into()?;
                sonar_report.extend(deny_report);
            }
            cli::Feature::Outdated => {
                use crate::outdated::Outdated;
                let outdated_report: sonar::Report =
                    Outdated::new(&options.outdated_path, &lockfile).try_into()?;
                sonar_report.extend(outdated_report);
            }
            cli::Feature::Udeps => {
                use crate::udeps::Udeps;
                let udeps_report: sonar::Report =
                    Udeps::new(&options.udeps_path, &lockfile).try_into()?;
                sonar_report.extend(udeps_report);
            }
        }
    }
    let file = File::create(options.sonar_path).context("failed to create 'sonar.json' file")?;
    info!("{} sonar issues created", sonar_report.len());
    #[cfg(not(debug_assertions))]
    let writer = serde_json::to_writer;
    #[cfg(debug_assertions)]
    let writer = serde_json::to_writer_pretty;
    writer(file, &sonar_report).context("failed to write sonar issues to 'sonar.json' file")?;
    Ok(())
}
