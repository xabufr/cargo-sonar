use crate::{cargo::Lockfile, sonar};
use eyre::{Context as _, Result};
use skip_error::SkipError;
use std::{
    fs::File,
    io::{BufRead as _, BufReader},
    path::PathBuf,
};
use tracing::info;

const DENY_ENGINE: &str = "deny";

#[derive(Debug, serde::Deserialize)]
pub struct GraphNode {
    name: String,
    version: String,
    #[serde(default)]
    kind: String,
    #[serde(default)]
    repeat: bool,
    #[serde(default)]
    parents: Vec<GraphNode>,
}

#[derive(Debug, serde::Deserialize)]
pub struct Label {
    line: usize,
    column: usize,
    message: String,
    span: String,
}

#[derive(Debug, serde::Deserialize)]
#[serde(untagged)]
enum DenyReport {
    Advisory {
        advisory: rustsec::advisory::Metadata,
    },
    License {
        code: String,
        graphs: Vec<GraphNode>,
        message: String,
        labels: Vec<Label>,
    },
}

#[derive(Debug)]
pub struct Deny<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

impl<'lock> Deny<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }
    fn to_issue(&self, deny_report: &DenyReport) -> sonar::Issue {
        match deny_report {
            DenyReport::Advisory { advisory } => {
                let rule_id = format!("{}::{}", DENY_ENGINE, advisory.id);
                let message = format!(
                    "{} (see https://github.com/rustsec/advisory-db/blob/main/crates/{}/{}.md)",
                    advisory.title, advisory.package, advisory.id
                );
                let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
                let text_range = self.lockfile.dependency_range(advisory.package.as_str());
                let primary_location = sonar::Location {
                    message,
                    file_path,
                    text_range,
                };
                let secondary_locations = Vec::new();
                sonar::Issue {
                    engine_id: DENY_ENGINE.to_string(),
                    rule_id,
                    severity: sonar::Severity::Major,
                    r#type: sonar::IssueType::Vulnerability,
                    primary_location,
                    secondary_locations,
                }
            }
            DenyReport::License {
                code,
                graphs,
                message,
                labels,
            } => {
                let graph_node = graphs.get(0).unwrap();
                let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
                let text_range = self.lockfile.dependency_range(graph_node.name.as_str());
                let message = labels.iter().fold(message.clone(), |message, label| {
                    format!("{}.\n`{}` {}", message, label.span, label.message)
                });
                let primary_location = sonar::Location {
                    message,
                    file_path,
                    text_range,
                };
                let secondary_locations = Vec::new();
                sonar::Issue {
                    engine_id: DENY_ENGINE.to_string(),
                    rule_id: format!("{}::{}", DENY_ENGINE, code),
                    severity: sonar::Severity::Info,
                    r#type: sonar::IssueType::CodeSmell,
                    primary_location,
                    secondary_locations,
                }
            }
        }
    }
}

impl std::convert::TryInto<sonar::Report> for Deny<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Report> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-deny' report from '{:?}' file",
                self.json
            )
        })?;
        let reader = BufReader::new(file);
        let issues: sonar::Report = reader
            .lines()
            .flatten()
            .flat_map(|line| serde_json::from_str::<serde_json::Value>(&line))
            .filter_map(|value| value.get("fields").map(ToString::to_string))
            .map(|s| {
                serde_json::from_str::<DenyReport>(&s)
                    .with_context(|| format!("failed to deserialize '{}'", s))
            })
            .skip_error_and_debug()
            .map(|diagnostic| self.to_issue(&diagnostic))
            .collect();
        info!("{} sonar issues created", issues.len());
        Ok(issues)
    }
}
