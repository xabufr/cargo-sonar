use crate::{cargo::Lockfile, sonar};
use eyre::{Context, Result};
use std::{fs::File, path::PathBuf};
use tracing::info;

const OUTDATED_ENGINE: &str = "outdated";

#[derive(Debug, serde::Deserialize)]
pub struct CrateMetadata {
    pub crate_name: String,
    pub dependencies: Vec<Metadata>,
}

#[derive(Debug, serde::Deserialize)]
pub struct Metadata {
    pub name: String,
    pub project: String,
    pub compat: String,
    pub latest: String,
    pub kind: Option<String>,
    pub platform: Option<String>,
}

#[derive(Debug)]
pub struct Outdated<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

impl<'lock> Outdated<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }

    #[allow(clippy::needless_pass_by_value)]
    fn to_issue(&self, metadata: Metadata) -> sonar::Issue {
        let message = format!(
            "'{}' is outdated and can be updated up to '{}'",
            metadata.name, metadata.latest
        );
        let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
        let text_range = self.lockfile.dependency_range(metadata.name.as_str());
        let primary_location = sonar::Location {
            message,
            file_path,
            text_range,
        };
        sonar::Issue {
            engine_id: OUTDATED_ENGINE.to_string(),
            rule_id: format!("{}::{}", OUTDATED_ENGINE, metadata.name),
            severity: sonar::Severity::Minor,
            r#type: sonar::IssueType::CodeSmell,
            primary_location,
            secondary_locations: Vec::new(),
        }
    }

    pub(crate) fn to_issues(&self, crate_metadata: CrateMetadata) -> sonar::Report {
        crate_metadata
            .dependencies
            .into_iter()
            .map(|dependency| self.to_issue(dependency))
            .collect()
    }
}

impl std::convert::TryInto<sonar::Report> for Outdated<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Report> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-outdated' report from '{:?}' file",
                self.json
            )
        })?;
        let crate_metadata = serde_json::from_reader::<_, CrateMetadata>(file)
            .context("failed to be parsed from JSON")?;
        let issues = self.to_issues(crate_metadata);
        info!("{} sonar issues created", issues.len());
        Ok(issues)
    }
}
