use crate::{cargo::Lockfile, sonar};
use eyre::{Context, Result};
use rustsec::{Report, Vulnerability};
use std::{fs::File, path::PathBuf};
use tracing::info;

const AUDIT_ENGINE: &str = "audit";

pub struct Audit<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

impl<'lock> Audit<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }

    #[allow(clippy::needless_pass_by_value)]
    fn to_issue(&self, vulnerability: Vulnerability) -> sonar::Issue {
        let advisory = &vulnerability.advisory;
        let rule_id = format!("{}::{}", AUDIT_ENGINE, advisory.id);
        let message = format!(
            "{} (see https://github.com/rustsec/advisory-db/blob/main/crates/{}/{}.md)",
            advisory.title, advisory.package, advisory.id
        );
        let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
        let text_range = self.lockfile.dependency_range(advisory.package.as_str());
        let primary_location = sonar::Location {
            message,
            file_path,
            text_range,
        };
        let secondary_locations = Vec::new();
        sonar::Issue {
            engine_id: AUDIT_ENGINE.to_string(),
            rule_id,
            severity: sonar::Severity::Major,
            r#type: sonar::IssueType::Vulnerability,
            primary_location,
            secondary_locations,
        }
    }
    pub fn to_issues(&self, report: Report) -> sonar::Report {
        report
            .vulnerabilities
            .list
            .into_iter()
            .map(|vulnerability| self.to_issue(vulnerability))
            .collect()
    }
}

impl std::convert::TryInto<sonar::Report> for Audit<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Report> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-audit' report from '{:?}' file",
                self.json
            )
        })?;
        let report = serde_json::from_reader::<_, Report>(file)
            .context("failed to be parsed as a 'rustsec::report::Report'")?;
        let issues = self.to_issues(report);
        info!("{} sonar issues created", issues.len());
        Ok(issues)
    }
}
