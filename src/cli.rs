use clap::Parser;
use eyre::bail;
use std::path::PathBuf;

#[derive(Debug)]
pub enum Feature {
    #[cfg(feature = "audit")]
    Audit,
    #[cfg(feature = "clippy")]
    Clippy,
    #[cfg(feature = "deny")]
    Deny,
    #[cfg(feature = "outdated")]
    Outdated,
    #[cfg(feature = "udeps")]
    Udeps,
}

impl std::str::FromStr for Feature {
    type Err = eyre::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let feature = match s.to_lowercase().as_str() {
            "audit" => Feature::Audit,
            "clippy" => Feature::Clippy,
            "deny" => Feature::Deny,
            "outdated" => Feature::Outdated,
            "udeps" => Feature::Udeps,
            _ => bail!("feature '{}' is not supported.", s),
        };
        Ok(feature)
    }
}

#[derive(Debug, Parser)]
#[clap(version, author)]
pub struct Command {
    #[clap(short, long, value_delimiter = ',')]
    pub features: Vec<Feature>,
    #[cfg(feature = "audit")]
    #[clap(long, default_value = "audit.json")]
    pub audit_path: PathBuf,
    #[cfg(feature = "clippy")]
    #[clap(long, default_value = "clippy.json")]
    pub clippy_path: PathBuf,
    #[cfg(feature = "deny")]
    #[clap(long, default_value = "deny.json")]
    pub deny_path: PathBuf,
    #[cfg(feature = "outdated")]
    #[clap(long, default_value = "outdated.json")]
    pub outdated_path: PathBuf,
    #[cfg(feature = "udeps")]
    #[clap(long, default_value = "udeps.json")]
    pub udeps_path: PathBuf,
    #[clap(short, long, default_value = "sonar.json")]
    pub sonar_path: PathBuf,
}
